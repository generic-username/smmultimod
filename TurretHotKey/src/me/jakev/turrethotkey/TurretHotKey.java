package me.jakev.turrethotkey;

import api.listener.Listener;
import api.listener.events.gui.HudCreateEvent;
import api.listener.events.input.KeyPressEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.mod.config.FileConfiguration;
import api.utils.game.PlayerUtils;
import api.utils.textures.StarLoaderTexture;
import org.schema.game.client.data.GameClientState;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.SegmentController;
import org.schema.schine.common.TextAreaInput;
import org.schema.schine.graphicsengine.core.ResourceException;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.graphicsengine.shader.Shader;
import org.schema.schine.graphicsengine.shader.ShaderLibrary;
import org.schema.schine.resource.ResourceLoader;

import java.io.IOException;

/**
 * Created by Jake on 7/17/2021.
 * <insert description here>
 */
public class TurretHotKey extends StarMod {
    public static Sprite onSprite;
    public static Sprite offSprite;
    public static boolean lastActive = false;

    public char onKey;
    public char offKey;

    @Override
    public void onResourceLoad(ResourceLoader loader) {
        onSprite = StarLoaderTexture.newSprite(getJarBufferedImage("me/jakev/turrethotkey/ON.png"), this, "thkON");
        offSprite = StarLoaderTexture.newSprite(getJarBufferedImage("me/jakev/turrethotkey/OFF.png"), this, "thkOFF");

        int scrPos = 32;
        onSprite.setPos(scrPos, scrPos, 0);
        offSprite.setPos(scrPos, scrPos, 0);
    }

    /*
        if (event.getChar() == 'j') {
                            ship.railController.getRoot().railController.activateAllAIClient(true, true, true);
                            GameClient.showPopupMessage("Turrets ON", 0);
                        } else if (event.getChar() == 'l') {
                            ship.railController.getRoot().railController.activateAllAIClient(false, true, false);
                            GameClient.showPopupMessage("Turrets OFF", 0);

                        }
         */
    int mcount = 0;
    @Override
    public void onEnable() {
        FileConfiguration config = getConfig("config");
        onKey = config.getConfigurableValue("OnKey", "j").charAt(0);
        offKey = config.getConfigurableValue("OffKey", "l").charAt(0);
        config.saveConfig();
        StarLoader.registerListener(HudCreateEvent.class, this, new Listener<HudCreateEvent>() {
            @Override
            public void onEvent(HudCreateEvent event) {
                event.elements.add(new THKHudElement(event.getInputState()));
            }
        });
        StarLoader.registerListener(KeyPressEvent.class, this, new Listener<KeyPressEvent>() {
            @Override
            public void onEvent(KeyPressEvent event) {
//                List<GUICallback> guiCallbacks = GameClientState.instance.getController().getInputController().getGuiCallbackController().getGuiCallbacks();
//                List<DialogInterface> playerInputs = GameClientState.instance.getController().getInputController().getPlayerInputs();
                TextAreaInput activeField = GameClientState.instance.getController().getInputController().getCurrentActiveField();
                boolean hasGuisActive = activeField != null;
                if(hasGuisActive){
                    return;
                }

                PlayerControllable control = PlayerUtils.getCurrentControl(GameClientState.instance.getPlayer());
                if (control instanceof SegmentController) {
                    SegmentController ship = (SegmentController) control;
                    if (event.getChar() == onKey) {
                        ship.railController.getRoot().railController.activateAllAIClient(true, true, true);
                        String message = "Turrets ON";
                        GameClientState.instance.getController().popupInfoTextMessage(message, "thk" + mcount++, 0);
                        lastActive = true;
                    } else if (event.getChar() == offKey) {
                        ship.railController.getRoot().railController.activateAllAIClient(false, true, false);
                        String message = "Turrets OFF";
                        GameClientState.instance.getController().popupInfoTextMessage(message, "thk" + mcount++, 0);
                        lastActive = false;
                    }
                }
            }
        });
    }
}
