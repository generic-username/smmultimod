package me.jakev.genpoint;

import api.mod.ModSkeleton;
import api.mod.StarLoader;
import api.mod.StarMod;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntCollection;
import org.ithirahad.resourcesresourced.RRSElementInfoManager;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.SpaceStation;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.server.ServerMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jake on 10/23/2021.
 * <insert description here>
 */
public class GenPoint extends StarMod {
    @Override
    public void onEnable() {
        GPConfig.init(this);
        GPConfigStr.init(this);
        new MsRunnable(this, GPConfig.ITEM_RESTOCK_TICK_MS.getValue()) {
            @Override
            public void runMs() {
                GameServerState inst = GameServerState.instance;
                if(inst != null){
                    try {
                        Sector sec = inst.getUniverse().getSector(new Vector3i(44, 44, 44));
                        if(sec != null){
                            for (SimpleTransformableSendableObject<?> entity : sec.getEntities()) {
                                if(entity instanceof SpaceStation) {
                                    if (entity.isHomeBase()) {
                                        //This is the entity
                                        handle((SpaceStation) entity);
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }
    public static boolean rrsEnabled(){
        ModSkeleton rrs = StarLoader.getLatestModFromId(8292);
        return rrs != null && rrs.isEnabled();
    }
    private static ArrayList<ElementInformation> oreTypes = new ArrayList<>();
    private static ArrayList<ElementInformation> getOreTypes(){
        if(!oreTypes.isEmpty()){
            return oreTypes;
        }
        try{
            if(rrsEnabled()) {
                long aegiumOre = RRSElementInfoManager.AEGIUM_ORE;
                System.err.println("Aegium ore: " + aegiumOre);
                oreTypes.clear();
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Aegium Ore"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Anbaric Vapor"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Crystalline Ore"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Ferron Ore"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Metallic Ore"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Parsyne Plasma"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Thermyn Amalgam"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Threns Relic"));
                oreTypes.add(RRSElementInfoManager.elementEntries.get("Floral Protomaterial"));
            }else{
                for (ElementInformation elem : ElementKeyMap.infoArray) {
                    if(elem != null && elem.isOre()){
                        oreTypes.add(elem);
                    }
                }
            }
        }catch (Exception e){
            System.err.println("[GenPoint] Could not load RRS ores");
            e.printStackTrace();
        }


        return oreTypes;
    }
    private static final IntCollection slotsTmp = new IntArrayList();
    public static void handle(SpaceStation ss){
        ArrayList<Inventory> invs = new ArrayList<>(ss.getInventories().inventoriesList);
        if(GPConfigStr.NAMED_INVENTORY_INSERT.getValue().length() > 3){
            String invName = GPConfigStr.NAMED_INVENTORY_INSERT.getValue();
            for (Inventory inv : ss.getInventories().inventoriesList) {
                if(inv.getCustomName() != null && inv.getCustomName().equalsIgnoreCase(invName)){
                    invs.clear();
                    invs.add(inv);
                    break;
                }
            }
        }
        int invDiv = ss.getInventories().inventoriesList.size();
        for (Inventory inv : invs) {
            for (ElementInformation oreType : getOreTypes()) {
                if(!inv.isFull()){
                    int slot = inv.incExistingOrNextFreeSlot(oreType.id, GPConfig.ITEM_AMOUNT.getValue() / invDiv);
                    slotsTmp.add(slot);
                }
            }
            inv.sendAll();
            slotsTmp.clear();
        }
        Object[] messages = new Object[]{GPConfigStr.BROADCAST_MESSAGE.getValue()};
        if(messages[0] == null || messages[0].toString().length() <= 2){
            return;
        }
        GameServerState.instance.getController().broadcastMessage(messages, ServerMessage.MESSAGE_TYPE_SIMPLE);
    }
}
