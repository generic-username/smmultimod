package me.jakev.eelite.listener;

import api.listener.Listener;
import api.listener.events.weapon.ExplosionEvent;
import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.MissileUpdateListener;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleUtil;
import me.jakev.eelite.EFXSprites;
import me.jakev.eelite.EFXColorUtil;
import me.jakev.eelite.net.PacketSCPlayExplosionParticle;
import me.jakev.eelite.particles.ExpandingRingParticle;
import me.jakev.eelite.particles.FadeScaleParticle;
import org.schema.game.common.data.missile.Missile;
import org.schema.game.common.data.missile.updates.MissileSpawnUpdate;
import org.schema.game.common.data.player.PlayerState;
import org.schema.schine.graphicsengine.core.Timer;

import javax.vecmath.Vector3f;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXMissileListener {
    public static void init(StarMod mod) {
        FastListenerCommon.missileUpdateListeners.add(new MissileUpdateListener() {
            @Override
            public void updateServer(Missile missile, Timer timer) {

            }

            @Override
            public void updateServerPost(Missile missile, Timer timer) {

            }

            Vector3f pos = new Vector3f();

            @Override
            public void updateClient(Missile missile, Timer timer) {
                pos.set(missile.getWorldTransform().origin);
            }

            @Override
            public void updateClientPost(Missile missile, Timer timer) {
                MissileSpawnUpdate.MissileType missileType = missile.getType();
                if (missileType == MissileSpawnUpdate.MissileType.BOMB) {
                    //no trail for bombs
                    return;
                }
                if (missileType == MissileSpawnUpdate.MissileType.HEAT) {
                    //Expanding ring
                    FadeScaleParticle particle = new FadeScaleParticle(5, 10, 20);
                    particle.lifetimeMs = 1000;
                    particle.startColor.set(EFXColorUtil.getV4FColorFromType(missile.getColorType()));
                    ModParticleUtil.playClient(missile.getSectorId(), missile.getWorldTransform().origin, EFXSprites.FLASH.getSprite(), particle);
                }else if (missileType == MissileSpawnUpdate.MissileType.FAFO || missileType == MissileSpawnUpdate.MissileType.DUMB) {
                    //Expanding ring
                    ExpandingRingParticle particle = new ExpandingRingParticle(2, 10);
                    particle.lifetimeMs = 1000;
                    particle.setArbitraryNormalOverride(missile.getDirection(new Vector3f()));
                    ModParticle.setColorF(particle, EFXColorUtil.getV4FColorFromType(missile.getColorType()));

                    ModParticleUtil.playClient(missile.getSectorId(), missile.getWorldTransform().origin, EFXSprites.RING1.getSprite(), particle);
                }
            }

        });


        StarLoader.registerListener(ExplosionEvent.class, new Listener<ExplosionEvent>() {
            @Override
            public void onEvent(ExplosionEvent event) {
                if (!event.isServer()) return;
                int sectorID = event.getSector().getSectorId();
                Vector3f toPos = event.getExplosion().centerOfExplosion.origin;

                PacketSCPlayExplosionParticle packet = new PacketSCPlayExplosionParticle(toPos, sectorID);
                for (PlayerState p : ModParticleUtil.getPlayersInRange(sectorID)) {
                    PacketUtil.sendPacket(p, packet);
                }

            }
        }, mod);
    }
}
