package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;
import api.utils.particle.ModParticleVertexBuffer;
import api.utils.particle.TextureSheetMergeAlgorithm;
import api.utils.particle.utils.ShipRelativeParticle;
import org.schema.game.common.controller.SegmentController;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.awt.*;
import java.util.Random;

/**
 * Created by Jake on 2023-04-24
 */

public class ChargeParticle extends ShipRelativeParticle {
    Vector3f startPos = new Vector3f();
    Vector3f endPos = new Vector3f();
    static Random r = new Random();
    public ChargeParticle(SegmentController segCon, Vector3f endPos, float rad) {
        super(segCon, endPos, endPos);
        Vector3f offset = new Vector3f((float) r.nextGaussian(), (float) r.nextGaussian(), (float) r.nextGaussian());
        offset.normalize();
        offset.scale(TextureSheetMergeAlgorithm.randFloat(rad));
        this.relativeVelocity.set(0,0,0);
        this.relativePosition.set(endPos);
        this.relativePosition.add(offset);

        this.startPos.set(relativePosition);
        this.endPos.set(endPos);
        lifetimeMs = 200+TextureSheetMergeAlgorithm.randInt(50);
    }


    Vector4f startColor = v4fFromColor(new Color(0, 140, 255, 0));
    Vector4f endColor = v4fFromColor(new Color(150, 233, 253, 255));
    @Override
    public void update(long currentTime) {
        positionOverTime(currentTime, startPos, endPos);
        super.update(currentTime);
        colorOverTime(this, currentTime, startColor, endColor);
        sizeOverTime(this, currentTime, 1.0F, 0.2F);
    }

    public void positionOverTime(long time, Vector3f start, Vector3f end){
        float endPercent = getLifetimePercent(time);
        float startPercent = 1F-endPercent;
        this.relativePosition.x = (start.x*startPercent + end.x*endPercent);
        this.relativePosition.y = (start.y*startPercent + end.y*endPercent);
        this.relativePosition.z = (start.z*startPercent + end.z*endPercent);
    }
    public static Vector4f v4fFromColor(Color c){
        Vector4f v = new Vector4f(c.getRed(), c.getGreen(), c.getBlue(), c.getAlpha());
        v.scale(1F/256F);
        return v;
    }
}
