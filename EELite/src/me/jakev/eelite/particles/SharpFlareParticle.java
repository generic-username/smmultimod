package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;
import api.utils.particle.utils.ShipRelativeParticle;
import org.schema.game.common.controller.SegmentController;

import javax.vecmath.Matrix3f;
import javax.vecmath.Vector3f;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class SharpFlareParticle extends ShipRelativeParticle {
    private float sizeGrow;

    public SharpFlareParticle(SegmentController segCon, Vector3f relativePosition, Vector3f relativeVelocity, Matrix3f relativeNormalOverride) {
        super(segCon, relativePosition, relativeVelocity, relativeNormalOverride);
    }


    @Override
    public void spawn() {
    }

    @Override
    public void update(long currentTime) {
        super.update(currentTime);
        fadeOverTime(this, currentTime);
    }
}
