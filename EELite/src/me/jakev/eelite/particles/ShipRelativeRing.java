package me.jakev.eelite.particles;

import api.utils.particle.utils.ShipRelativeParticle;
import org.schema.game.common.controller.SegmentController;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
import java.awt.*;

/**
 * Created by Jake on 2023-04-24
 */

public class ShipRelativeRing extends ShipRelativeParticle {
    private final float startSize;
    private final float endSize;
    static Vector4f startColor = ChargeParticle.v4fFromColor(new Color(71, 255, 255));
    static Vector4f endColor = ChargeParticle.v4fFromColor(new Color(113, 113, 255));

    public boolean useColor = true;

    public ShipRelativeRing(SegmentController segCon, Vector3f relativePosition, float startSize, float endSize) {
        super(segCon, relativePosition, new Vector3f());
        this.startSize = startSize;
        this.endSize = endSize;
    }

    @Override
    public void update(long currentTime) {
        super.update(currentTime);
        if(useColor){
            colorOverTime(this, currentTime, startColor, endColor);
        }else{
            fadeOverTime(this, currentTime);
        }
        sizeOverTime(this, currentTime, startSize, endSize);
    }
}
