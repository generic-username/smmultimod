package me.jakev.eelite.particles;

import api.utils.particle.ModParticle;
import api.utils.particle.TextureSheetMergeAlgorithm;

import javax.vecmath.Vector4f;

/**
 * Created by Jake on 12/8/2020.
 * Particle that fades and scales over time
 */
public class FadeScaleParticle extends ModParticle {
    public Vector4f startColor = new Vector4f(1,1,1,1);
    public Vector4f endColor = new Vector4f(1,1,1,0);
    private final float startSize;
    private final float endSize;
    private int randomLife;

    public FadeScaleParticle(float startSize, float endSize, int randomLife) {
        this.startSize = startSize;
        this.endSize = endSize;
        this.randomLife = randomLife;
    }

    @Override
    public void spawn() {
        super.spawn();
        if(randomLife != 0) {
            lifetimeMs += TextureSheetMergeAlgorithm.randInt(randomLife);
        }
    }

    @Override
    public void update(long currentTime) {
        colorOverTime(this, currentTime, startColor, endColor);
        sizeOverTime(this, currentTime, startSize, endSize);
    }
}
