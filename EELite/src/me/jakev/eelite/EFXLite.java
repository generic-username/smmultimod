package me.jakev.eelite;

import api.config.BlockConfig;
import api.mod.StarMod;
import api.network.packets.PacketUtil;
import api.utils.particle.ModParticleUtil;
import api.utils.textures.StarLoaderTexture;
import me.jakev.eelite.listener.*;
import me.jakev.eelite.net.PacketSCOverheatEvent;
import me.jakev.eelite.net.PacketSCPlayExplosionParticle;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

/**
 * Created by Jake on 1/28/2022.
 * <insert description here>
 */
public class EFXLite extends StarMod {
    public static StarMod inst;

    @Override
    public void onLoadModParticles(ModParticleUtil.LoadEvent event) {
        EFXSprites.init(this, event);
    }

    @Override
    public void onEnable() {
        inst = this;
        EFXBarListener.init(this);
        EFXMissileListener.init(this);
        EFXBeamListener.init(this);
        EFXCannonListener.init(this);
        EFXSoundListener.init(this);
        EFXExplosionListener.init(this);
        PacketUtil.registerPacket(PacketSCPlayExplosionParticle.class);
        PacketUtil.registerPacket(PacketSCOverheatEvent.class);
    }

/*    @Override
    public void onBlockConfigLoad(BlockConfig config) {
        StarLoaderTexture myTexture = StarLoaderTexture.newBlockTexture(getJarBufferedImage("me/jakev/eelite/tex.png"), getJarBufferedImage("me/jakev/eelite/texnrm.png"));

        ElementInformation myBlockInfo = BlockConfig.newElement(this, "NrmTestBlock", (short) myTexture.getTextureId());
        myBlockInfo.setBuildIconNum(ElementKeyMap.getInfo(ElementKeyMap.DECORATIVE_PANEL_1).getBuildIconNum());
        myBlockInfo.setShoppable(true);
        myBlockInfo.setPrice(1);
        BlockConfig.add(myBlockInfo);

    }*/
}
