package me.jakev.uploadmycrashes;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Created by Jake on 10/20/2022.
 * <insert description here>
 */
public class LogServer {
    private byte[] buf = new byte[256];
    DatagramSocket socket;

    public LogServer() {
        try {
            socket = new DatagramSocket(5252);
            run();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() throws IOException {
        while (true){
            DatagramPacket packet
                    = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            InetAddress address = packet.getAddress();
            int port = packet.getPort();
            packet = new DatagramPacket(buf, buf.length, address, port);
            String received = new String(packet.getData(), 0, packet.getLength());
            System.err.println("printing the entire goddamn log file: " + received);
            socket.close();
        }
    }
}
