package me.jakev.uploadmycrashes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.*;
import java.util.Scanner;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Jake on 10/20/2022.
 * <insert description here>
 */
public class LogHandler extends Handler {

    @Override
    public void publish(LogRecord record) {
    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
        System.err.println("--------- CLOSED LOG!!!!!!!!!!!!!!!!");
        File logFile = new File("logs/logstarmade.0.log");
        StringBuilder sb= new StringBuilder();
        try {
            Scanner scanner = new Scanner(logFile);
            while (scanner.hasNext()){
                sb.append(scanner.nextLine()).append("\n");
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        try {
            byte[] data = sb.toString().getBytes();
            DatagramSocket socket = new DatagramSocket();
            DatagramPacket packet
                    = new DatagramPacket(data, data.length, InetAddress.getByName("localhost"), 5252);
            socket.send(packet);

        } catch (SocketException e) {
            throw new RuntimeException(e);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
