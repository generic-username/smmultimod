package me.jakev.emb;

import api.config.BlockConfig;
import api.listener.fastevents.FastListenerCommon;
import api.mod.StarMod;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;

/**
 * Created by Jake on 10/30/2021.
 * <insert description here>
 */
public class ExtraMB extends StarMod {
    public static ElementInformation block;
    public static int div;
    public static int capsuleCraftingAmount;
    public static short craftingType;
    @Override
    public void onBlockConfigLoad(BlockConfig blockConfig) {
        ElementInformation powCap = ElementKeyMap.getInfo(ElementKeyMap.POWER_CAP_ID);
        block = BlockConfig.newElement(this, "Resource Amplifier", powCap.getTextureIds());
        BlockConfig.setBasicInfo(block, "yeah", 1000, 0.1F, true, false, powCap.getBuildIconNum());
        BlockConfig.add(block);

        //Tekt capsule
        BlockConfig.addRecipe(block, 3, 5, new FactoryResource(capsuleCraftingAmount, craftingType));
    }

    @Override
    public void onEnable() {
        FastListenerCommon.statusEffectApplyListeners.add(new MiningBonusListener());
        div = getConfig("config").getConfigurableInt("BlocksPerOneBonus", 200);
        capsuleCraftingAmount = getConfig("config").getConfigurableInt("capsuleCraftingAmount", 1000);
        craftingType = (short) getConfig("config").getConfigurableInt("craftingType", 204);
    }
}
