package me.jakev.salvageturrets;

import api.listener.Listener;
import api.listener.events.block.SegmentPieceSalvageEvent;
import api.listener.events.weapon.BeamPostAddEvent;
import api.listener.fastevents.FastListenerCommon;
import api.listener.fastevents.ShipAIEntityAttemptToShootListener;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.ai.CustomAITargetUtil;
import it.unimi.dsi.fastutil.objects.ObjectIterator;
import org.schema.game.client.data.GameClientState;
import org.schema.game.common.controller.FloatingRock;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.beam.harvest.SalvageBeamHandler;
import org.schema.game.common.data.SimpleGameObject;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.server.ai.AIControllerStateUnit;
import org.schema.game.server.ai.ShipAIEntity;
import org.schema.game.server.ai.program.turret.states.SeachingForTurretTarget;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.network.objects.Sendable;

import javax.annotation.Nullable;
import javax.vecmath.Vector3f;

/**
 * Created by Jake on 7/26/2021.
 * <insert description here>
 */
public class SalvageTurrets extends StarMod {
    @Override
    public void onEnable() {
        CustomAITargetUtil.registerNewTargetProgram("Asteroids", new CustomAITargetUtil.FindTargetProgram() {
            @Nullable
            @Override
            public SimpleTransformableSendableObject<?> findTarget(SeachingForTurretTarget state) {
                if (state.getEntityState().getState().getUpdateTime() - state.lastCheck > 3000) {
                    state.getEntityState().lastEngage = "";
                    if (GameClientState.isDebugObject(state.getEntity())) {
                        System.err.println("[AI][TURRET] FIND ANY TARGET: " + state.getEntity() + "; " + state.lastCheck);
                    }



                    state.lastCheck = state.getEntityState().getState().getUpdateTime();
//						System.err.println("FIND ANY TARGET --->: "+state.getEntity());
                    state.possibleEnemies.clear();
                    float minLen = 1000000;
                    //				System.err.println("NOW SEARCHING TARGET FOR "+getGObj().getSendable());
                    synchronized (state.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalObjects()) {

                        for (Sendable s : state.getEntityState().getState().getLocalAndRemoteObjectContainer().getLocalUpdatableObjects().values()) {
                            if (s instanceof SimpleTransformableSendableObject) {
                                
                                if (s == state.getEntity()) {
                                    if (GameClientState.isDebugObject(state.getEntity())) {
                                        System.err.println("[AI][DEBUG] " + state.getEntity() + " dismissed " + s + " as target: target is self");
                                    }
                                    continue;
                                }

                                if (!(s instanceof FloatingRock)) {
                                    if (GameClientState.isDebugObject(state.getEntity())) {
                                        System.err.println("[AI][DEBUG] " + state.getEntity() + " dismissed " + s + " as target: not an asteroid");
                                    }
                                    continue;
                                }
                                ((SimpleTransformableSendableObject) s).calcWorldTransformRelative(state.getEntity().getSectorId(), ((GameServerState) state.getEntity().getState()).getUniverse().getSector(state.getEntity().getSectorId()).pos);
                                state.dist.sub(((SimpleTransformableSendableObject) s).getClientTransform().origin, state.getEntity().getWorldTransform().origin);

//						System.err.println("DISTANCE TO "+s+": "+dist);

                                if (state.dist.length() < state.getEntityState().getShootingRange() - 10 /*&& dist.length() < minLen*/) {
                                    if (GameClientState.isDebugObject((SimpleTransformableSendableObject) s)) {
                                        System.err.println("[AI][DEBUG] " + state.getEntity() + " added as possible enemy " + s + ". " + state.dist.length() + " / range: " + (state.getEntityState().getShootingRange() - 100));
                                    }
                                    minLen = state.dist.length();
                                    state.possibleEnemies.add(((SimpleTransformableSendableObject) s));
                                } else {
                                    if (GameClientState.isDebugObject((SimpleTransformableSendableObject) s)) {
                                        System.err.println("[AI][DEBUG] " + state.getEntity() + " dismissed " + s + " as target: distance too far: dist: " + state.dist.length() + " / range: " + (state.getEntityState().getShootingRange() - 100));
                                    }
                                }
                            }

                        }
                    }
                }
                if (state.possibleEnemies.size() > 0) {
                    //			System.err.println("POSSIBLE ENEMIES: "+possibleEnemies);
//                    ((TargetProgram<?>) state.getEntityState().getCurrentProgram()).setTarget();
                    SimpleGameObject entity = state.possibleEnemies.get(0);

                    return (SimpleTransformableSendableObject<?>) entity;
                }
                return null;
            }
        });
        FastListenerCommon.shipAIEntityAttemptToShootListeners.add(new ShipAIEntityAttemptToShootListener() {
            @Override
            public void doShooting(ShipAIEntity entity, AIControllerStateUnit<?> unit, Timer timer) {
                entity.getEntity().getManagerContainer().getSalvage().getElementManager().handle(unit, timer);
            }
        });
        StarLoader.registerListener(BeamPostAddEvent.class, this, new Listener<BeamPostAddEvent>() {
            Vector3f randVecTmp = new Vector3f();
            short seed = 12347;
            public int random() {
                seed ^= (seed << 4);
                seed ^= (seed >>> 8);
                seed ^= (seed << 2);
                return seed;
            }
            void randomizeVec(){
                randVecTmp.set(random(), random(), random());
                randVecTmp.scale(0.001F);
            }
            @Override
            public void onEvent(BeamPostAddEvent event) {
                if(event.getHandler() instanceof SalvageBeamHandler){
                    if(!event.getBeamState().mouseButton[0]){
                        randomizeVec();
                        event.getBeamState().to.add(randVecTmp);
                    }
                }
            }
        });
        StarLoader.registerListener(SegmentPieceSalvageEvent.class, this, new Listener<SegmentPieceSalvageEvent>() {
            @Override
            public void onEvent(SegmentPieceSalvageEvent event) {
                SegmentController turret = event.getSegmentController();
                if(turret.isDocked()){
                    SegmentController rootEntity = turret.railController.getRoot();
                    if(rootEntity instanceof ManagedUsableSegmentController){
                        ManagedUsableSegmentController<?> base = (ManagedUsableSegmentController<?>) rootEntity;
                        ObjectIterator<Inventory> iterator = base.getInventories().values().iterator();
                        if(iterator.hasNext()){
                            int miningBonus = base.getMiningBonus(event.getSegmentPiece().getSegmentController());
                            Inventory inv = iterator.next();
                            int slot = inv.incExistingOrNextFreeSlot(event.getSegmentPiece().getType(), 1);
                            base.getManagerContainer().sendInventoryDelayed(inv, slot);
                            if (ElementKeyMap.hasResourceInjected(event.getSegmentPiece().getType(), event.getOrientation()) && inv.canPutIn(ElementKeyMap.orientationToResIDMapping[event.getOrientation()], 1 * miningBonus)) {
                                int slotOre = inv.incExistingOrNextFreeSlot(ElementKeyMap.orientationToResIDMapping[event.getOrientation()], 1 * miningBonus);
                                base.getManagerContainer().sendInventoryDelayed(inv, slotOre);
                            }
                        }
                    }
                }
            }
        });
    }
}
