package me.jakev.realdebree;

import api.listener.events.Event;

/**
 * Created by Jake on 7/11/2021.
 * <insert description here>
 */
public class RealDebreeFloatingItemDropPrepareEvent extends Event {
    private RealDebree.FloatingItemDrop itemDrop;

    public RealDebreeFloatingItemDropPrepareEvent(RealDebree.FloatingItemDrop itemDrop) {
        this.itemDrop = itemDrop;
    }

    public RealDebree.FloatingItemDrop getItemDrop() {
        return itemDrop;
    }

    public void setItemDrop(RealDebree.FloatingItemDrop itemDrop) {
        this.itemDrop = itemDrop;
    }
}
