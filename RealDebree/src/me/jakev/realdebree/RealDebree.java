package me.jakev.realdebree;

import api.listener.Listener;
import api.listener.events.Event;
import api.listener.events.block.SegmentPieceKillEvent;
import api.listener.events.player.PlayerPickupFreeItemEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import api.utils.StarRunnable;
import api.utils.game.PlayerUtils;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.controller.ManagedUsableSegmentController;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.data.SegmentPiece;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.inventory.FreeItem;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.game.common.data.world.SegmentData;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.server.ServerMessage;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;

public class RealDebree extends StarMod {
    private static final Vector3f halfDimVec = new Vector3f(SegmentData.SEG_HALF, SegmentData.SEG_HALF, SegmentData.SEG_HALF);

    @Override
    public void onEnable() {
        System.err.println("Enabled RealDebree (epically)");
        StarLoader.registerListener(SegmentPieceKillEvent.class, new Listener<SegmentPieceKillEvent>() {
            @Override
            public void onEvent(SegmentPieceKillEvent event) {
                if (GameServerState.instance != null) {
                    SegmentPiece piece = event.getPiece();
                    SegmentController segCon = piece.getSegmentController();
                    RemoteSector secServer = getServerSideRemoteSector(segCon);
                    Vector3f shipPos = piece.getAbsolutePos(new Vector3f());
                    shipPos.sub(halfDimVec);
                    segCon.getWorldTransform().transform(shipPos);
                    short itemType = piece.getType();

                    // Do not drop cores into space (they overheat)
                    if (itemType == ElementKeyMap.CORE_ID) {
                        return;
                    }

                    FloatingItemDrop e = new FloatingItemDrop(itemType, secServer, shipPos);
                    // Fire event
                    RealDebreeFloatingItemDropPrepareEvent ev = new RealDebreeFloatingItemDropPrepareEvent(e);
                    StarLoader.fireEvent(ev, true);
                    e = ev.getItemDrop();
                    ///

                    itemDrops.add(e);
//                    secServer.addItem(shipPos, itemType, -1, 1);
                }
            }
        }, this);

        new StarRunnable() {
            @Override
            public void run() {
                processDropMap();
            }
        }.runTimer(this, 100);

        StarLoader.registerListener(PlayerPickupFreeItemEvent.class, this, new Listener<PlayerPickupFreeItemEvent>() {
            @Override
            public void onEvent(PlayerPickupFreeItemEvent event) {
                if (event.getCondition() == Event.Condition.PRE) {
                    PlayerState player = event.getPlayer();
                    PlayerControllable control = PlayerUtils.getCurrentControl(player);
                    if (control instanceof ManagedUsableSegmentController<?>) {
                        ManagedUsableSegmentController<?> ms = (ManagedUsableSegmentController<?>) control;
                        ObjectArrayList<Inventory> segInventories = ms.getManagerContainer().getInventories().inventoriesList;
                        Inventory toPut = null;
                        for (Inventory inv : segInventories) {
                            if (inv.getCustomName() != null) {
                                toPut = inv;
                                break;
                            }
                        }
                        //Put item in inventory instead
                        if (toPut != null) {
                            FreeItem item = event.getItem();
                            int slot = toPut.incExistingOrNextFreeSlot(item.getType(), item.getCount(), item.getMetaId());
                            player.sendInventoryModification(slot, Long.MIN_VALUE);
                            String message = "Picked up " + ElementKeyMap.getInfo(item.getType()).getName() + "(" + item.getCount() + ")" + " in storage: " + toPut.getCustomName();
                            player.sendServerMessage(
                                    new ServerMessage(
                                            new Object[]{message},
                                            ServerMessage.MESSAGE_TYPE_INFO, player.getId()));
                            GameServerState.instance.getUniverse().getSector(player.getSectorId()).getRemoteSector().removeItem(item.getId());
                            event.setCanceled(true);
                        }
                    }
                }
            }
        });

    }

    /**
     * StarMade netcode strikes again
     * <p>
     * In singleplayer: every in-game sector has 2 remote sectors, server and client.
     * addItem must be called on the SERVER remote sector, otherwise the player cannot pickup the item.
     * <p>
     * On a dedicated server+client: The server needs to call addItem
     */
    public static RemoteSector getServerSideRemoteSector(SegmentController controller) {
        if (GameServerState.instance != null) {
            return GameServerState.instance.getUniverse().getSector(controller.getSectorId()).getRemoteSector();
        } else {
            return controller.getRemoteSector();
        }
    }

    public static ConcurrentLinkedDeque<FloatingItemDrop> itemDrops = new ConcurrentLinkedDeque<>();

    private static void processDropMap() {
        //Sort by remote sector
        HashMap<RemoteSector, ArrayList<FloatingItemDrop>> dropMap = new HashMap<>();
        for (FloatingItemDrop drop : itemDrops) {
            ArrayList<FloatingItemDrop> drops = dropMap.get(drop.sectorId);
            if (drops == null) {
                ArrayList<FloatingItemDrop> newArr = new ArrayList<>();
                dropMap.put(drop.sectorId, newArr);
                drops = newArr;
            }
            drops.add(drop);
        }
        for (Map.Entry<RemoteSector, ArrayList<FloatingItemDrop>> entry : dropMap.entrySet()) {
            RemoteSector sec = entry.getKey();
            ArrayList<FloatingItemDrop> items = entry.getValue();
            //Batch into types
            HashMap<Short, ArrayList<FloatingItemDrop>> typeMap = new HashMap<>();
            for (FloatingItemDrop item : items) {
                ArrayList<FloatingItemDrop> drops = typeMap.get(item.type);
                if (drops == null) {
                    drops = new ArrayList<>();
                    typeMap.put(item.type, drops);
                }
                drops.add(item);
            }
            for (Map.Entry<Short, ArrayList<FloatingItemDrop>> typeEntry : typeMap.entrySet()) {
                Short type = typeEntry.getKey();
                ArrayList<FloatingItemDrop> typeItems = typeEntry.getValue();
                //Calculate average position, a weird way to do it but it works
                int avgX = 0, avgY = 0, avgZ = 0;
                for (FloatingItemDrop item : typeItems) {
                    avgX += item.pos.x;
                    avgY += item.pos.y;
                    avgZ += item.pos.z;
                }
                avgX /= typeItems.size();
                avgY /= typeItems.size();
                avgZ /= typeItems.size();
                sec.addItem(new Vector3f(avgX, avgY, avgZ), type, -1, typeItems.size());
            }
        }
        itemDrops.clear();
    }

    public static class FloatingItemDrop {
        private short type;
        private RemoteSector sectorId;
        private Vector3f pos;

        public FloatingItemDrop(short type, RemoteSector sectorId, Vector3f pos) {
            this.type = type;
            this.sectorId = sectorId;
            this.pos = pos;
        }

        public short getType() {
            return type;
        }

        public RemoteSector getSectorId() {
            return sectorId;
        }

        public Vector3f getPos() {
            return pos;
        }

        public void setType(short type) {
            this.type = type;
        }

        public void setSectorId(RemoteSector sectorId) {
            this.sectorId = sectorId;
        }

        public void setPos(Vector3f pos) {
            this.pos = pos;
        }
    }
}
