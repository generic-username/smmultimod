package me.jakev.nostationdecay;

import api.listener.Listener;
import api.listener.events.entity.SegmentControllerSpawnEvent;
import api.mod.StarLoader;
import api.mod.StarMod;

/**
 * Created by Jake on 10/14/2021.
 * <insert description here>
 */
public class NoStationDecay extends StarMod {
    @Override
    public void onEnable() {
        StarLoader.registerListener(SegmentControllerSpawnEvent.class, this, new Listener<SegmentControllerSpawnEvent>() {
            @Override
            public void onEvent(SegmentControllerSpawnEvent event) {
                event.getController().setScrap(false);
                event.getController().getNetworkObject().scrap.set(false);
            }
        });
    }
}
