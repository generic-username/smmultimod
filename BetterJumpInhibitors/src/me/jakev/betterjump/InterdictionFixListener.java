package me.jakev.betterjump;

import api.listener.Listener;
import api.listener.events.Event;
import api.listener.events.systems.InterdictionCheckEvent;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.common.controller.elements.jumpprohibiter.InterdictionAddOn;
import org.schema.game.common.controller.elements.power.reactor.tree.ReactorTree;
import org.schema.game.common.data.ManagedSegmentController;
import org.schema.game.common.data.blockeffects.config.StatusEffectType;
import org.schema.game.common.data.world.Sector;
import org.schema.game.common.data.world.SimpleTransformableSendableObject;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;

public class InterdictionFixListener extends Listener<InterdictionCheckEvent> {
    public void onEvent(Event event) {

    }
    public static int getStrength(InterdictionAddOn addOn){
        return addOn.getConfigManager().apply(StatusEffectType.WARP_INTERDICTION_STRENGTH, 1);
    }
    public static int getDistance(InterdictionAddOn addOn){
        return addOn.getConfigManager().apply(StatusEffectType.WARP_INTERDICTION_DISTANCE, 1);
    }
    @Override
    public void onEvent(InterdictionCheckEvent e) {
        GameServerState server = GameServerState.instance;
        if (server != null) {
            if(!(e.getSegmentController() instanceof ManagedSegmentController)){
                return;
            }
            ManagedSegmentController jumper = (ManagedSegmentController) e.getSegmentController();
            Universe universe = server.getUniverse();
            for (int x = -3; x <= 3; x++) {
                for (int y = -3; y <= 3; y++) {
                    for (int z = -3; z <= 3; z++) {
                        Vector3i v = e.getSegmentController().getSector(new Vector3i());
                        v.add(x, y, z);
                        Sector sector = universe.getSectorWithoutLoading(v);
                        if (sector != null) {
                            for (SimpleTransformableSendableObject<?> entity : sector.getEntities()) {
                                if (entity instanceof ManagedSegmentController) {
                                    ManagedSegmentController ship = (ManagedSegmentController) entity;
                                    InterdictionAddOn inter = ship.getManagerContainer().getInterdictionAddOn();
                                    if (inter.isActive()) {
                                        int strength = getStrength(inter);
                                        int distance = getDistance(inter);
                                        ReactorTree activeReactor = ship.getManagerContainer().getPowerInterface().getActiveReactor();

                                        int inderdictorLevel = activeReactor.getLevel();
                                        int jumperMax = jumper.getManagerContainer().getPowerInterface().getActiveReactor().getLevel();
                                        int extraDelta = strength * 20;
                                        Vector3i sectorPosition = new Vector3i(v);
                                        Vector3i shipSecPos = ship.getSegmentController().getSector(new Vector3i());
                                        sectorPosition.sub(shipSecPos);
                                        double d = sectorPosition.length();
                                        if (d <= distance &&
                                                inderdictorLevel + extraDelta > jumperMax)
                                            e.setInterdicted(true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
