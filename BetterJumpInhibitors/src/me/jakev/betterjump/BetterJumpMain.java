package me.jakev.betterjump;

import api.common.GameServer;
import api.listener.Listener;
import api.listener.events.entity.ShipJumpEngageEvent;
import api.listener.events.systems.InterdictionCheckEvent;
import api.mod.StarLoader;
import api.mod.StarMod;
import org.schema.game.client.data.PlayerControllable;
import org.schema.game.common.data.player.PlayerState;
import org.schema.game.common.data.player.faction.Faction;
import org.schema.game.common.data.player.faction.FactionManager;
import org.schema.game.common.data.player.faction.FactionRelation;
import org.schema.game.common.data.world.Universe;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.network.RegisteredClientOnServer;

import java.io.IOException;

public class BetterJumpMain extends StarMod {
    public static final int WARP_INTER_1 = 1093;

    public void onEnable() {
        StarLoader.registerListener(InterdictionCheckEvent.class, this, new InterdictionFixListener());
        StarLoader.registerListener(ShipJumpEngageEvent.class, this, new Listener<ShipJumpEngageEvent>() {
            public void onEvent(ShipJumpEngageEvent e) {
                boolean currentSectorIsEnemy, destSectorEnemy;
                try {
                    FactionManager fm = GameServerState.instance.getFactionManager();
                    Universe universe = GameServerState.instance.getUniverse();
                    Faction ownerFaction = fm.getFaction(universe.getSector(e.getOriginalSectorPos())._getSystem().getOwnerFaction());
                    Faction destOwner = fm.getFaction(universe.getSector(e.getNewSector())._getSystem().getOwnerFaction());

                    Faction shipFaction = fm.getFaction(((PlayerControllable) e.getController()).getAttachedPlayers().get(0).getFactionId());
                    if (shipFaction == null)
                        return;
                    if (ownerFaction == null) {
                        currentSectorIsEnemy = false;
                    } else {
                        currentSectorIsEnemy = (ownerFaction.getRelationshipWithFactionOrPlayer(shipFaction.getIdFaction()) == FactionRelation.RType.ENEMY);
                    }
                    if (destOwner == null) {
                        destSectorEnemy = false;
                    } else {
                        destSectorEnemy = (destOwner.getRelationshipWithFactionOrPlayer(shipFaction.getIdFaction()) == FactionRelation.RType.ENEMY);
                    }
                    if (currentSectorIsEnemy || destSectorEnemy) {
                        e.setCanceled(true);
                        for (PlayerState p : ((PlayerControllable) e.getController()).getAttachedPlayers())
                            sendMessage(p, "Cannot jump in or out of enemy system");
                    }
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
    }

    public static void sendMessage(PlayerState player, String message) {
        RegisteredClientOnServer registeredClientOnServer = GameServer.getServerState().getClients().get(player.getClientId());
        try {
            registeredClientOnServer.serverMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
