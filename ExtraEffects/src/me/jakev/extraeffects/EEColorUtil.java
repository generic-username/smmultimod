package me.jakev.extraeffects;

import org.schema.game.client.view.tools.ColorTools;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;

import javax.vecmath.Vector4f;

/**
 * Created by Jake on 7/18/2021.
 * <insert description here>
 */
public class EEColorUtil {
    public static final Vector4f DEFAULT_COLOR = new Vector4f(1,1,1,1);
    public static Vector4f getV4FColorFromType(int type){
        ElementInformation info = ElementKeyMap.getInfoFast(type);
        if (info != null && info.isLightSource()) {
            Vector4f color = new Vector4f();
            color.set(info.getLightSourceColor());
            ColorTools.brighten(color);
            ColorTools.brighten(color);
            ColorTools.brighten(color);
            return color;
        }
        return DEFAULT_COLOR;
    }
}
