package me.jakev.extraeffects.listeners;

/**
 * STARMADE MOD
 * CREATOR: Max1M
 * DATE: 10.07.2021
 * TIME: 17:41
 * interface required by the RemotePlay packet.
 * !!if you use this you HAVE to test if the client is in range of the original sector. otherwise everyone can see the particle, no matter
 * what sector they are in
 */
public interface Playable {
    void play();
}
